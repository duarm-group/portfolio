# Duarm portfolio

Contact: duarm0@disroot.org

Github: https://github.com/duarm

You can find most of my old projects there

## Projects

- [sakyra](https://gitlab.com/melonworks/sakyra "sakyra") - A c99 game engine, with a vulkan backend, 
audio and more. With support for windows and linux (WIP).

- [catchme](https://gitlab.com/kurenaiz/catchme "Catchme") - A CLI for communicating with MPV on linux

- [mpv-catchme](https://gitlab.com/kurenaiz/mpv-catchme "mpv-catchme") - MPV lua script which 
adds music-server-like features to mpv

- [catchme-scripts](https://gitlab.com/kurenaiz/catchme-scripts "catchme-scripts") - General convenience scripts to use with catchme

- [tomoyo-theme.nvim](https://gitlab.com/kurenaiz/tomoyo-vim-theme "catchme-scripts") - Dark Brown/Green theme for Neovim

- [satool](https://gitlab.com/kurenaiz/satool "satool") - atool but written in lua

## Learning projects

### Backend

- [Sakura DB](https://gitlab.com/kurenaiz/sakuradb "sakuradb") - Rest API for the anime Cardcaptor Sakura, written in Go.

### Frontend

- [Sakura DB Web](https://gitlab.com/kurenaiz/sakuradb-web "sakuradb web") - Simple frontend for Sakura DB, 
written in pure and simple Javascript.

- [Sakura DB App](https://gitlab.com/duarm-group/sakuradb_flutter "sakuradb app") - Flutter frontend for Sakura DB

### Unity

- [Horizon](https://github.com/duarm/Horizon "Horizon") - Solar System orbit viewer and a planet viewer, 
made as a TCC for my Computers highschool technical course.

- [Match3](https://github.com/duarm/Match3 "match3") - Simple Match3 game made with Unity

- [LightsOff](https://github.com/duarm/lightsoff "lightsoff") - Simplest LightsOff game made with Unity 

- [Seega](https://github.com/duarm/Seega "seega") - Simple seega player, an african board game, made with Unity

- [Congehou Sun Days](https://github.com/duarm/Congehou-Sun-Days "congehou") - Bullet hell game for a 4 day game jam with Unity

- [Unity Utils](https://github.com/duarm/unity-utils "unity utils") - Useful unity functions I use.

### Games

- [out-doctors](https://github.com/duarm/out-doctors "out-doctors") - Simple game infinite-runner game made with raylib for a 3 day gamejam.

- [Harebell & The Mystery Misty](https://gitlab.com/kurenaiz/mahou_shoujo/-/tree/master) - Simple game made to test my Sakyra game engine, for a 3 day gamejam.

